package com.example.jintarkop;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {



    ImageButton button_aboutus;


    public HomeFragment () {
        // Required empty public constructor
    }

    EditText in_kontrol1, in_kontrol2;
    Button btn_kontrol1, btn_kontrol2;
    DatabaseReference dbkontrol, dbstatus;
    TextView txt_status;



    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container ,
                               Bundle savedInstanceState ) {

        View v = inflater.inflate ( R.layout.fragment_home , container , false );

        in_kontrol1 = v.findViewById ( R.id.kontrol1 );
        in_kontrol2 = v.findViewById ( R.id.kontrol2 );
        btn_kontrol1 = v.findViewById ( R.id.btn_kontrol1 );
        btn_kontrol2 = v.findViewById ( R.id.btn_kontrol2 );
        txt_status = v.findViewById ( R.id.txt_status );

//        //membaca status perangkat
//        dbstatus = FirebaseDatabase.getInstance ().getReference ();
//        dbstatus.child ( "pengering1" ).addValueEventListener ( new ValueEventListener () {
//            @Override
//            public void onDataChange ( @NonNull DataSnapshot dataSnapshot ) {
//                String Notifikasi =dataSnapshot.child ( "Notifikasi" ).getValue ().toString ();
//                txt_status.setText ( Notifikasi );
//            }
//
//            @Override
//            public void onCancelled ( @NonNull DatabaseError databaseError ) {
//
//            }
//        } );


        //kontrol1
        btn_kontrol1.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( in_kontrol1.getText ().toString () )){
                    dbkontrol = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    dbkontrol.child ( "kontrol1" ).setValue ( in_kontrol1.getText ().toString () );
                    Toast.makeText ( getActivity () , "Kontrol1 Berhasil Diinput" , Toast.LENGTH_SHORT ).show ();
                } else {
                    in_kontrol1.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Kontrol1 Kosong" , Toast.LENGTH_SHORT ).show ();
                }
            }
        } );

        //kontrol2
        btn_kontrol2.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( in_kontrol2.getText ().toString () )){
                    dbkontrol = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    dbkontrol.child ( "kontrol2" ).setValue ( in_kontrol2.getText ().toString () );
                    Toast.makeText ( getActivity () , "Kontrol2 Berhasil Diinput" , Toast.LENGTH_SHORT ).show ();
                } else {
                    in_kontrol2.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Kontrol2 Kosong" , Toast.LENGTH_SHORT ).show ();
                }
            }
        } );

        //button about us
        button_aboutus = v.findViewById ( R.id.btn_aboutus );
        button_aboutus.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
               Intent in = new Intent ( getActivity (), AboutUs.class );
               in.putExtra ( "some", "Aplikasi Jintarkop" );
               startActivity ( in );

            }
        } );

        return v;

    }

}
